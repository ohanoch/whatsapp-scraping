import os
import wget
import tarfile

with open("src/mysettings.txt", "w") as settings_file:
    settings_file.write("[config]\n")
    browser = input("choose browser: firefox or chrome (chrome is default)\n")
    settings_file.write("BROWSER = " + browser + "\n")
    browser_path = os.path.join(os.path.expanduser('~'), ".config/google-chrome/Default")
    if browser == "firefox":
        ls = os.listdir(os.path.join(os.path.expanduser('~'), ".mozilla/firefox/"))
        for f in ls:
            if f.endswith("default-release"):
                browser_path = os.path.expanduser('~') + "/.mozilla/firefox/" + f
        wget.download("https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz", "/tmp")
        gecko_file = tarfile.open("/tmp/geckodriver-v0.29.1-linux64.tar.gz")
        gecko_file.extractall(os.path.join(os.path.expanduser('~'), ".local/bin"))
        print("\n")
    else:
        print("browser given is: " + browser + "using chrome")
    settings_file.write("BROWSER_PATH = " + browser_path + "\n")
    name = input("enter name of contact/group to read from\n")
    settings_file.write("NAME = " + name + "\n")
    settings_file.write("PAGE = https://web.whatsapp.com/\n")

    
