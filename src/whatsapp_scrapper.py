"""
Importing the libraries that we are going to use
for loading the settings file and scraping the website
"""

from selenium import webdriver
from selenium.common.exceptions import (NoSuchElementException,
                                        StaleElementReferenceException)
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from bs4 import BeautifulSoup
import re

class WhatsappScrapper():
    def __init__(self, page, browser, browser_path):
        self.page = page
        self.browser = browser
        self.browser_path = browser_path
        self.driver = self.load_driver()

        # Open the web page with the given browser
        self.driver.get(self.page)

    def load_driver(self):
        """
        Load the Selenium driver depending on the browser
        (Edge and Safari are not running yet)
        """
        driver = None
        if self.browser == 'firefox':
            firefox_profile = webdriver.FirefoxProfile(
                self.browser_path)
            driver = webdriver.Firefox(firefox_profile)
        elif self.browser == 'chrome':
            chrome_options = webdriver.ChromeOptions()
            if self.browser_path:
                chrome_options.add_argument('user-data-dir=' +
                                            self.browser_path)
            driver = webdriver.Chrome(options=chrome_options)
        elif self.browser == 'safari':
            pass
        elif self.browser == 'edge':
            pass

        return driver

    def open_conversation(self, name):
        """
        Function that search the specified user by the 'name' and opens the conversation.
        """
        scroll = 0
        target = self.driver.find_element_by_id('pane-side') #self.driver.find_elements_by_xpath("//div[@id='pane-side']/div/div/div/div")#self.driver.find_element_by_id('pane-side')
        self.driver.execute_script(f"arguments[0].scrollTo(0, 0)", target)
        while True:
            self.driver.execute_script(f"arguments[0].scrollTop = {scroll}", target)
            source = self.driver.page_source
            soup = BeautifulSoup(source, 'html.parser')
            left_panel = soup.findAll("div", {"id": "pane-side"})[0] #This will fetch all the left side panel's code
            left_panel_soup = BeautifulSoup(str(left_panel), 'html.parser')
            #chat_div_list = left_panel_soup.findAll('div', {'tabindex' : '-1'})[1:] #This _list will contain all the chats div , 0th index is ignored([1:]) because it will be parent div with all the children div in it
            try:
                self.driver.find_element_by_xpath(f"//span[text()='{name}']").click()
                element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, "main")))
                print("found chat \"" + name + "\"!")
                return True
            except:
                print("chat NOT found \"" + name + "\"!")
                return False
                
            """
            #for chatter in self.driver.find_elements_by_xpath("//div[@id='pane-side']/div/div/div/div"):
            print("--- going through list of chats with scroll " + str(scroll) + " ---")
            for chat_div in chat_div_list:
                curr_chat_name = chat_div.find('span', {'title': True})['title']
                #print(curr_chat_name)
                if curr_chat_name == name:
                    self.driver.find_element_by_xpath(f"//span[text()='{curr_chat_name}']").click()
                    element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, "main")))
                    print("found chat \"" + name + "\"!")
                    return True
            
            scroll += 350
            if scroll > 100000:
                return False
                #chatter_path = ".//span[@title='{}']".format(
                #    name)
            """
            """
                # Wait until the chatter box is loaded in DOM
                try:
                    print("000000000000000000")
                    WebDriverWait(self.driver, 10).until(
                        EC.presence_of_element_located(
                            (By.XPATH, "//span[contains(@title,'{}')]".format(
                                name)))
                    )
                    print("1111111111111")
                except StaleElementReferenceException:
                    print("222222222222222\t" + str(e))
                    WebDriverWait(self.driver, 10).until(
                        EC.presence_of_element_located(
                            (By.XPATH, "//span[contains(@title,'{}')]".format(
                                name)))
                    )
                    print("3333333333333333")
            """
            """
                try:
                    print("66666666666666")
                    chatter_name = chatter.find_element_by_xpath(
                        chatter_path).text
                    print("444444444444444444444")
                    if chatter_name == name:
                        print("55555555555555555555555")
                        chatter.find_element_by_xpath(
                            ".//div/div").click()
                        return True
                except Exception as e:
                    print(e)
                    pass
           """
    def read_last_in_messages(self):
        """
        Reading the last message that you got in from the chatter
        """
        source = self.driver.page_source
        soup = BeautifulSoup(source, 'html.parser')
        all_soup = soup.find('div', {"id" : "main"})
        soup = BeautifulSoup(str(all_soup), 'html.parser')
        filtered_soup = soup.find('div', {"class" : "copyable-area"})
        filtered_soup = list(filtered_soup)[2]
        soup = BeautifulSoup(str(filtered_soup), 'html.parser')
        final_soup = soup.findAll('div',{"class" : "copyable-text"})     #This has all divs for each message which has information if the message is replied to or normal message

        messages_since_last = []
        #for messages in self.driver.find_elements_by_xpath(
        #        "//div[contains(@class,'message-in')]"):
        for message in final_soup:
            cleanr = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
            cleantext = re.sub(cleanr, '', str(message))
            if cleantext == "--- Messeges copied to csv ---":
                messages_since_last = []
                continue

            messages_since_last.append(cleantext)
            """
            try:
                message = ""
                emojis = []

                message_container = messages.find_element_by_xpath(
                    ".//div[@class='copyable-text']")

                message = message_container.find_element_by_xpath(
                    ".//span[contains(@class,'selectable-text invisible-space copyable-text')]"
                ).text

                for emoji in message_container.find_elements_by_xpath(
                        ".//img[contains(@class,'selectable-text invisible-space copyable-text')]"
                ):
                    emojis.append(emoji.get_attribute("data-plain-text"))
                if messege == "--- Messeges copied to csv ---":
                    messeges_since_last = []
                else:
                    messeges_since_last.append((messege,emojis))
            except NoSuchElementException:  # In case there are only emojis in the message
                try:
                    message = ""
                    emojis = []
                    message_container = messages.find_element_by_xpath(
                        ".//div[contains(@class,'copyable-text')]")

                    for emoji in message_container.find_elements_by_xpath(
                            ".//img[contains(@class,'selectable-text invisible-space copyable-text')]"
                    ):
                        emojis.append(emoji.get_attribute("data-plain-text"))
                    if message == "--- Messeges copied to csv ---":
                        messages_since_last = []
                    else:
                        messages_since_last.append((message,emojis))
                except NoSuchElementException:
                    pass
            """
        return messages_since_last

    def send_message(self, text):
        """
        Send a message to the chatter.
        You need to open a conversation with open_conversation()
        before you can use this function.
        """

        input_text = self.driver.find_element_by_xpath(
            "//div[@id='main']/footer/div/div[2]/div/div[@contenteditable='true']")

        input_text.click()
        input_text.send_keys(text)

        send_button = self.driver.find_element_by_xpath(
            "//div[@id='main']/footer/div/div[3]/button")
        send_button.click()

        return True
