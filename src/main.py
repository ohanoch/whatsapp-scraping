import configparser
import time

from whatsapp_scrapper import WhatsappScrapper

import os
import csv
import re

def load_settings():
    """
    Loading and assigning global variables from our settings.txt file
    """
    config_parser = configparser.RawConfigParser()
    config_file_path = 'src/mysettings.txt'
    config_parser.read(config_file_path)

    browser = config_parser.get('config', 'BROWSER')
    browser_path = config_parser.get('config', 'BROWSER_PATH')
    name = config_parser.get('config', 'NAME')
    page = config_parser.get('config', 'PAGE')

    settings = {
        'browser': browser,
        'browser_path': browser_path,
        'name': name,
        'page': page
    }
    return settings


def main():
    """
    Loading all the configuration and opening the website
    (Browser profile where whatsapp web is already scanned)
    """
    if not os.path.isfile("src/mysettings.txt"):
        with open("src/mysettings.txt", "w") as settings_file:
            settings_file.write("[config]\n")
            browser = input("choose browser: firefox or chrome (chrome is default)\n")
            settings_file.write("BROWSER = " + browser + "\n")
            browser_path = os.path.join(os.path.expanduser('~'), ".config/google-chrome/Default")
            if browser == "firefox":
                ls = os.listdir(os.path.join(os.path.expanduser('~'), ".mozilla/firefox/"))
                for f in ls:
                    if f.endswith("default-release"):
                        browser_path = os.path.expanduser('~') + "/.mozilla/firefox/" + f
                else:
                    print("browser given is: " + browser + "using chrome")
                settings_file.write("BROWSER_PATH = " + browser_path + "\n")
                name = input("enter name of contact/group to read from\n")
                settings_file.write("NAME = " + name + "\n")
                settings_file.write("PAGE = https://web.whatsapp.com/\n")

    settings = load_settings()
    print(settings)
    scrapper = WhatsappScrapper(
        settings['page'], settings['browser'], settings['browser_path'])
    print("sleeping 10")
    time.sleep(10)
    print("sleep end")
    if scrapper.open_conversation(settings['name']):
        if not os.path.isfile(settings['name'].replace(" ", "_") + ".csv"):
            print("creating file: " + settings['name'].replace(" ","_") + ".csv")
            with open(settings['name'].replace(" ","_") + ".csv", "w") as new_csv:
                writer = csv.writer(new_csv)
                writer.writerow(["Name", "Amount", "Price Per Unit", "Price Total", "Currency"])
                scrapper.open_conversation(settings['name'])
                scrapper.send_message("--- CSV file created, please write messages with currency attached to price  ---")
                
        while True:
            with open(settings['name'].replace(" ","_") + ".csv", "a+") as outfile:
                print("opened file: " + settings['name'] + ".csv")
                writer = csv.writer(outfile)
                messages = scrapper.read_last_in_messages()
                for message in messages:
                    print(str(message))
                    texts = str(message).split("\n")
                    for text in texts:
                        numbers = re.findall('\d*\.?\d+', text)
                        #print(str(numbers))
                        if len(numbers) != 0:
                            if "$" in text:
                                currency = "$"
                            elif "euro" in text.lower():
                                currency = "euro"
                            elif "nis" in text.lower():
                                currency = "nis"
                            else:
                                print("no currency found, skipping message: " + text)
                                continue
                            
                            amount = 1
                            price_total = "???"
                            name = ""
                            for word in text.split(" "):
                                if currency in word:
                                    price_total = re.findall('\d*\.?\d+', word)[0]
                                elif re.findall('\d*\.?\d+', word) != []:
                                    amount = re.findall('\d*\.?\d+', word)[0]
                                else:
                                    if name == "":
                                        name = word
                                    else:
                                        name += " " + word
                            if price_total == "???":
                                print("skipped message because no price found: " + text)
                                continue

                            price_per_unit = str(float(price_total)/float(amount))

                            writer.writerow([name, amount, price_per_unit, price_total, currency])
                            print("added message: " + text)
                        else:
                            print("skipped message because no numbers found: " + text)
                    outfile.close()
                if messages != []:
                    scrapper.open_conversation(settings['name'])
                    scrapper.send_message("--- Messeges copied to csv ---")
                time.sleep(60*60)
    else:
        print("conversation not found: " + settings['name'])
if __name__ == '__main__':
    main()
